# Commission Calculator

This is a REST API built with express to calculate commission rate whe transferring money

## How to run the API

1. First, you need a running postgres database and fill in the necessary database configurations in the .env file. Please check `.env.example` for reference. Or you can use docker with this script: `./scripts/start-db.sh`

2. Once the database is running, start the app with this command: `yarn run start:dev`. This command will watch for file change and run the server accordingly

3. To run unit test: `yarn run test:unit`

4. To run integration test: `yarn run test:int`. Make sure you have postgres running.
