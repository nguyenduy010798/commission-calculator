import request from 'supertest'
import { createConnection } from 'typeorm'

import app from '../src/app'
import { Transaction } from '../src/entities/Transaction'

describe('commission controller', () => {
  beforeAll(async () => {
    await createConnection({
      type: 'postgres',
      host: 'localhost',
      port: 5432,
      username: 'postgres',
      password: 'secret',
      database: 'commission',
      synchronize: true,
      logging: false,
      entities: [Transaction],
    })
      .then(() => console.log('connected'))
      .catch(err => console.log('err', err))
  })

  it('should calculate the correct commission', async () => {
    const payload = {
      date: new Date().toDateString(),
      amount: '100',
      currency: 'EUR',
      client_id: 10,
    }

    const response = await request(app)
      .post('/api/v1/commission/calculate')
      .send(payload)

    expect(response.statusCode).toEqual(200)
    expect(response.body.amount).toEqual(`${0.005 * Number(payload.amount)}`)
  })
})
