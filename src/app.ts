import express from 'express'
import cors from 'cors'

import commissionRouter from './routers/commission'
import errorHandler from './middlewares/errorHandler'

const prefix = '/api/v1'
export const isProd = process.env.NODE_ENV === 'production'

const app = express()

app.use(cors())
app.use(express.json())
app.use(`${prefix}/commission`, commissionRouter)
app.use(errorHandler)

export default app
