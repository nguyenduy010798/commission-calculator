import express from 'express'

import { commissionHandler } from '../controllers/commission'

const router = express.Router()

router.post('/calculate', commissionHandler)

export default router
