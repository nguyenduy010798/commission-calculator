export type CalculateCommissionPayload = {
  date: string
  amount: string
  currency: string
  client_id: number
}

export type CalculateCommissionResponse = {
  amount: string
  currency: string
}

export type CurrencyRateResponse = {
  rates: {
    [key: string]: number
  }
}
