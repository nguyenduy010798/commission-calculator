import { BaseEntity, Column, Entity, PrimaryGeneratedColumn } from 'typeorm'

@Entity()
export class Transaction extends BaseEntity {
  @PrimaryGeneratedColumn('uuid')
  id!: string

  @Column()
  amount!: number

  @Column()
  date!: Date

  @Column()
  currency!: string

  @Column()
  clientId!: number
}
