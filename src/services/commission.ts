import axios from 'axios'
import { Transaction } from '../entities/Transaction'
import {
  CalculateCommissionPayload,
  CalculateCommissionResponse,
  CurrencyRateResponse,
} from '../types'

import { InternalServerError } from '../util/error'

const saveTransaction = async ({
  amount,
  client_id,
  date,
  currency,
}: CalculateCommissionPayload) => {
  try {
    const transaction = new Transaction()
    transaction.amount = Number(amount)
    transaction.clientId = client_id
    transaction.date = new Date(date)
    transaction.currency = currency

    await transaction.save()
  } catch (error) {
    throw new InternalServerError()
  }
}

const calculateCommission = async (
  payload: CalculateCommissionPayload
): Promise<CalculateCommissionResponse> => {
  const { currency, date, client_id } = payload
  let amount = Number(payload.amount)
  let commission: number

  commission = 0.005 * amount
  if (commission < 0.05) {
    commission = 0.05
  }

  if (client_id === 42) {
    commission = 0.05
  }

  try {
    if (currency !== 'EUR') {
      amount = await convertCurrency(amount, currency)
    }

    const averageTransAmountPerMonth =
      await calculateAverageTransactionAmountPerMonth(client_id)

    if (averageTransAmountPerMonth >= 1000) {
      commission = 0.03
    }

    return {
      amount: commission.toString(),
      currency: 'EUR',
    }
  } catch (error) {
    throw new InternalServerError()
  }
}

const convertCurrency = async (
  amount: number,
  currency: string
): Promise<number> => {
  const currencyRatesApi = 'https://api.exchangerate.host/latest'
  try {
    const { data } = await axios.get<CurrencyRateResponse>(currencyRatesApi)
    const rate = data.rates[currency]
    return amount / rate
  } catch (error) {
    throw new InternalServerError()
  }
}

const calculateAverageTransactionAmountPerMonth = async (
  clientId: number
): Promise<number> => {
  try {
    const allTransactions = await Transaction.find({ clientId })

    if (allTransactions.length === 0) {
      return 0
    }

    if (allTransactions.length === 1) {
      return allTransactions[0].amount
    }

    const totalAmount = allTransactions.reduce(
      (acc, next) => acc + next.amount,
      0
    )

    const sorted = allTransactions.sort((a, b) => {
      const A = new Date(a.date).toDateString()
      const B = new Date(b.date).toDateString()

      if (Date.parse(A) > Date.parse(B)) {
        return 1
      }

      if (Date.parse(A) === Date.parse(B)) {
        return 0
      }

      return -1
    })

    const firstTransaction = sorted[0]
    const lastTransaction = sorted[sorted.length - 1]
    const diff =
      Math.abs(new Date(lastTransaction.date).getTime()) -
      Math.abs(new Date(firstTransaction.date).getTime())
    const diffMonths = Math.ceil(diff / (1000 * 3600 * 24 * 30))

    return totalAmount / diffMonths
  } catch (error) {
    throw new InternalServerError()
  }
}

export default {
  saveTransaction,
  calculateCommission,
  calculateAverageTransactionAmountPerMonth,
  convertCurrency,
}
