import commissionService from './commission'
import { Transaction } from '../entities/Transaction'

const { calculateCommission } = commissionService

describe('commission service', () => {
  it('should calculate the correct commission with EUR currency at default rate', async () => {
    const mockTransactions = [
      {
        id: '1234',
        amount: 50,
        date: '10-Jan-2020',
        currency: 'EUR',
        clientId: 1,
      },
    ]

    jest.spyOn(Transaction, 'find').mockReturnValue(mockTransactions as any)

    const commission = await calculateCommission({
      date: new Date().toDateString(),
      amount: '100',
      currency: 'EUR',
      client_id: 1,
    })

    expect(commission).toEqual({ amount: `${0.005 * 100}`, currency: 'EUR' })
  })

  it('should calculate the correct commission with EUR currency at default rate when commission is less than 0.05e', async () => {
    const mockTransactions = [
      {
        id: '1234',
        amount: 50,
        date: '10-Jan-2020',
        currency: 'EUR',
        clientId: 1,
      },
    ]

    jest.spyOn(Transaction, 'find').mockReturnValue(mockTransactions as any)

    const commission = await calculateCommission({
      date: new Date().toDateString(),
      amount: '5',
      currency: 'EUR',
      client_id: 1,
    })

    expect(commission).toEqual({ amount: '0.05', currency: 'EUR' })
  })

  it('should calculate the correct commission with client id of 42', async () => {
    const mockTransactions = [
      {
        id: '1234',
        amount: 50,
        date: '10-Jan-2020',
        currency: 'EUR',
        clientId: 1,
      },
    ]

    jest.spyOn(Transaction, 'find').mockReturnValue(mockTransactions as any)

    const commission = await calculateCommission({
      date: new Date().toDateString(),
      amount: '100',
      currency: 'EUR',
      client_id: 42,
    })

    expect(commission).toEqual({ amount: '0.05', currency: 'EUR' })
  })

  it('should calculate the correct commission with client that has transaction avg over 1000e', async () => {
    const mockTransactions = [
      {
        id: '1234',
        amount: 1200,
        date: '10-Jan-2020',
        currency: 'EUR',
        clientId: 1,
      },
      {
        id: '1234',
        amount: 1500,
        date: '10-Feb-2020',
        currency: 'EUR',
        clientId: 1,
      },
    ]

    jest.spyOn(Transaction, 'find').mockReturnValue(mockTransactions as any)

    const commission = await calculateCommission({
      date: new Date().toDateString(),
      amount: '100',
      currency: 'EUR',
      client_id: 2,
    })

    expect(commission).toEqual({ amount: '0.03', currency: 'EUR' })
  })
})
