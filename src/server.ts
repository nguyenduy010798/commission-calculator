import dotenv from 'dotenv'
import { createConnection, ConnectionOptions } from 'typeorm'

import app from './app'
import { Transaction } from './entities/Transaction'

dotenv.config()

const typeormConfig: ConnectionOptions = {
  type: 'postgres',
  host: process.env.DB_HOST,
  port: Number(process.env.DB_PORT as string),
  username: process.env.DB_USERNAME as string,
  password: process.env.DB_PASSWORD as string,
  database: process.env.DB_DATABASE as string,
  synchronize: true,
  logging: false,
  entities: [Transaction],
}

createConnection(typeormConfig)
  .then(() => console.log('connected to postgres'))
  .catch(err => console.error(`failed to connect to postgres: ${err}`))

app.listen(process.env.PORT || 5000)
