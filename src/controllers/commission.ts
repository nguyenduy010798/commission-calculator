import { Request, Response, NextFunction } from 'express'

import CommissionService from '../services/commission'
import { CalculateCommissionPayload } from '../types'
import { InternalServerError } from '../util/error'

export const commissionHandler = async (
  req: Request,
  res: Response,
  next: NextFunction
) => {
  try {
    const payload = req.body as CalculateCommissionPayload
    await CommissionService.saveTransaction(payload)
    const result = await CommissionService.calculateCommission(payload)
    res.json(result)
  } catch (error) {
    next(new InternalServerError())
  }
}
